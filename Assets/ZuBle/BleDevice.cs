﻿namespace ZuBle
{
    public record BleDevice(string Address, string Name);
}