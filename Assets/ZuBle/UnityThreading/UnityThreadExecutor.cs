﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using UnityEngine;

namespace ZuBle.UnityThreading
{
    public class UnityThreadExecutor : MonoBehaviour
    {
        private static GameObject currentExecutorObject;
        private static UnityThreadExecutor currentExecutor;
        
        private readonly BlockingCollection<Action> _updateActions = new();

        private static bool isAfterUpdateAndBeforeLateUpdate;
        private static int mainThreadId;

        private static bool CanExecuteNow =>
            isAfterUpdateAndBeforeLateUpdate && Thread.CurrentThread.ManagedThreadId == mainThreadId;
        
        private static UnityThreadExecutor GetExecutor()
        {
            if (currentExecutor is not null && currentExecutor)
            {
                return currentExecutor;
            }

            throw new Exception("Call initialize within the Unity Thread first.");
        }

        public static void Initialize()
        {
            if (currentExecutorObject)
            {
                Destroy(currentExecutor);
                currentExecutor = null;
                Destroy(currentExecutorObject);
                currentExecutorObject = null;
            }
            
            currentExecutorObject = new GameObject("UnityThreadExecutor");
            currentExecutor = currentExecutorObject.AddComponent<UnityThreadExecutor>();
            mainThreadId = Thread.CurrentThread.ManagedThreadId;
        }
        
        private void Update()
        {
            isAfterUpdateAndBeforeLateUpdate = true;
            
            if (_updateActions.Count <= 0)
            {
                return;
            }

            while (_updateActions.Count > 0)
            {
                if (_updateActions.TryTake(out var action))
                {
                    action?.Invoke();
                }
            }
        }

        private void LateUpdate()
        {
            isAfterUpdateAndBeforeLateUpdate = false;
        }
        
        public static void AddActionOnUpdate(Action action)
        {
            if (CanExecuteNow)
            {
                action();
            }
            else
            {
                GetExecutor()._updateActions.Add(action);
            }
        }
        
        public static void AddActionOnUpdate<T>(Action<T> action, T param)
        {
            AddActionOnUpdate(() => action.Invoke(param));
        }

        public static void AddActionOnUpdate<T1, T2>(Action<T1, T2> action, T1 param1, T2 param2)
        {
            AddActionOnUpdate(() => action.Invoke(param1, param2));
        }

        public static void AddActionOnUpdate<T1, T2, T3>(Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
        {
            AddActionOnUpdate(() => action.Invoke(param1, param2, param3));
        }
    }
}