﻿using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace ZuBle.Listeners
{
    // ReSharper disable InconsistentNaming
    public class ZuBleConnectionListener : AndroidJavaProxy
    {
        public event Action<string> OnConnected;
        public event Action<string> OnDisconnected;
        public event Action<string> OnServicesDiscovered;
        public event Action<string, int> OnServiceDiscoveryFailed;

        private readonly AsyncTaskQueue<string> connectedPendingTasks = new();
        private readonly AsyncTaskQueue<bool> mtuChangePendingTasks = new();
        private readonly AsyncTaskQueue<bool> writeCharacteristicPendingTasks = new();
        
        public event Action<string, int> OnMtuChanged;
        public event Action<string, int> OnMtuChangeFailed;
        
        public ZuBleConnectionListener() : base(Java.WithPackage("BLEConnectionCallback")) {}

        public void AddPendingConnectionTask(UniTaskCompletionSource<string> taskSource)
        {
            connectedPendingTasks.Add(taskSource);
        }

        public void AddMtuChangeTask(UniTaskCompletionSource<bool> mtuTask)
        {
            mtuChangePendingTasks.Add(mtuTask);
        }

        [UsedImplicitly, Preserve]
        public void WriteCharacteristicTask(UniTaskCompletionSource<bool> writeTask)
        {
            writeCharacteristicPendingTasks.Add(writeTask);
        }
        
        [UsedImplicitly, Preserve]
        public void onConnected(string deviceAddress)
        {
            OnConnected?.Invoke(deviceAddress);
#if ZUBLE_DEBUG
            Debug.Log($"Connected to device: {deviceAddress}");
#endif
            connectedPendingTasks.Complete(deviceAddress);
        }

        [UsedImplicitly, Preserve]
        public void onServicesDiscovered(string deviceAddress)
        {
            OnServicesDiscovered?.Invoke(deviceAddress);
#if ZUBLE_DEBUG
            Debug.Log("Services discovered");
#endif
        }

        [UsedImplicitly, Preserve]
        public void onServicesDiscoveryFailed(string deviceAddress, int status)
        {
            OnServiceDiscoveryFailed?.Invoke(deviceAddress, status);
            Debug.LogWarning($"Service discovery failed: {status}");
        }
        
        [UsedImplicitly, Preserve]
        public void onDisconnected(string deviceAddress)
        {
            OnDisconnected?.Invoke(deviceAddress);
#if ZUBLE_DEBUG
            Debug.Log($"Disconnected from device: {deviceAddress}");
#endif
            connectedPendingTasks.CancelAll();
            mtuChangePendingTasks.CancelAll();
        }

        [UsedImplicitly, Preserve]
        public void onMtuChangeSuccess(string deviceAddress, int mtu)
        {
            Debug.Log($"MTU changed successfully {deviceAddress}: mtu {mtu}");
            OnMtuChanged?.Invoke(deviceAddress, mtu);
            mtuChangePendingTasks.Complete(true);
        }

        [UsedImplicitly, Preserve]
        public void onMtuChangeFailed(string deviceAddress, int status)
        {
            Debug.LogWarning($"MTU changed failed {deviceAddress}: status {status}");
            OnMtuChangeFailed?.Invoke(deviceAddress, status);
            mtuChangePendingTasks.Complete(false);
        }
    }
}