﻿using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;

namespace ZuBle.Listeners
{
    // ReSharper disable InconsistentNaming
    public class ZuBleWriteListener : AndroidJavaProxy
    {
        public event WriteCallback OnCharacteristicWritten;
        public event CharacteristicOperationFailedCallback OnCharacteristicWriteFailed;

        private readonly AsyncTaskQueue writeCharacteristicPendingTasks = new();
        
        public ZuBleWriteListener() : base(Java.WithPackage("BLEWriteCallback")) {}
        
        public void AddWriteTask(UniTaskCompletionSource taskSource){
            writeCharacteristicPendingTasks.Add(taskSource);
        }
        
        [UsedImplicitly]
        public bool onCharacteristicWritten(string deviceAddress, string characteristicUUid)
        {
            OnCharacteristicWritten?.Invoke(deviceAddress, characteristicUUid);
            return writeCharacteristicPendingTasks.Complete();
        }

        [UsedImplicitly]
        public bool onCharacteristicWriteFailed(string deviceAddress, string characteristicUUid, int status)
        {
            OnCharacteristicWriteFailed?.Invoke(deviceAddress, characteristicUUid, status);
            return writeCharacteristicPendingTasks.CancelAll();
        }
    }
}