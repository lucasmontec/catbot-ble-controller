﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace ZuBle.Listeners
{
    public class ZuBleDeviceFoundListener : AndroidJavaProxy
    {
        public event Action<BleDevice> OnDeviceFound;
        
        public ZuBleDeviceFoundListener() : base(Java.WithPackage("BLEDeviceFoundCallback"))
        {
        }
        
        [UsedImplicitly, Preserve]
        // ReSharper disable once InconsistentNaming
        public void onDeviceFound(string deviceName, string deviceAddress)
        {
#if ZUBLE_DEBUG
            Debug.Log($"Device Found: Name = {deviceName}, Address = {deviceAddress} event is null: {OnDeviceFound == null}");
#endif
            OnDeviceFound?.Invoke(new BleDevice(deviceAddress, deviceName));
        }
    }
}