﻿namespace ZuBle
{
    public delegate void WriteCallback(string deviceAddress, string characteristicUUid);
    public delegate void ReadCallback(string deviceAddress, string characteristicId, byte[] value);
    public delegate void CharacteristicOperationFailedCallback(string deviceAddress, string characteristicId, int status);
}