﻿
## Tasks

 * Join input state into a single packet instead of individual input messages.

 * ~~Create command struct and change controllers to send commands.~~
 * ~~Create command queue to avoid trying to send multiple commands when a send fails. Algorithm:~~
   * ~~Try to send the command. If it fails, add the command to the queue.~~
   * ~~On the next frame, try to send all commands again until the list is clear.~~