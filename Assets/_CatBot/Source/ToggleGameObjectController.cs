﻿using UnityEngine;
using UnityEngine.UI;

namespace _CatBot
{
    public class ToggleGameObjectController : MonoBehaviour
    {
        [SerializeField]
        private Toggle toggle;

        [SerializeField]
        private GameObject enabledWhenOn;
        
        [SerializeField]
        private GameObject enabledWhenOff;

        public bool forceTankMode;
        
        private void OnEnable()
        {
            if (forceTankMode)
            {
                toggle.isOn = true;
                toggle.gameObject.SetActive(false);
                UpdateObjectsFromValue(true);
                return;
            }
            
            toggle.onValueChanged.AddListener(OnToggleChanged);
            UpdateObjectsFromValue(toggle.isOn);
        }

        private void OnDisable()
        {
            toggle.onValueChanged.RemoveListener(OnToggleChanged);
        }

        private void OnToggleChanged(bool value)
        {
            UpdateObjectsFromValue(value);
        }

        private void UpdateObjectsFromValue(bool value)
        {
            enabledWhenOn.SetActive(value);
            enabledWhenOff.SetActive(!value);
        }
    }
}