﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _CatBot
{
    public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public bool IsPressed { get; private set; }

        public event Action WhilePressed;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            IsPressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            IsPressed = false;
        }
        
        private void Update()
        {
            if (IsPressed)
            {
                WhilePressed?.Invoke();
            }
        }
    }
}