﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _CatBot
{
    public class PointerEventsSlider : Slider
    {
        public event Action<PointerEventData> PointerDown; 
        public event Action<PointerEventData> PointerUp;

        public int resetValue;

        public void ResetSlider()
        {
            value = resetValue;
        }
        
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            
            PointerDown?.Invoke(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            
            PointerUp?.Invoke(eventData);
        }
    }
}