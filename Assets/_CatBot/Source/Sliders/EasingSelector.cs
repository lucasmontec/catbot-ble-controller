﻿using System;
using _CatBot.Math;
using TMPro;
using UnityEngine;

namespace _CatBot.Sliders
{
    public class EasingSelector : MonoBehaviour
    {
        private const string PrefsKey = "easek";
        public TMP_Dropdown dropdown;
        public SliderCommandController[] sliderCommandControllers;

        private void Awake()
        {
            dropdown.options.Clear();
            
            foreach (var easingType in Enum.GetNames(typeof(Easing.Ease)))
            {
                dropdown.options.Add(new TMP_Dropdown.OptionData(easingType));
            }

            var lastValue = PlayerPrefs.GetInt(PrefsKey, 21);
            dropdown.value = lastValue;
            OnDropdownValueChanged(lastValue);
        }

        private void OnEnable()
        {
            dropdown.onValueChanged.AddListener(OnDropdownValueChanged); 
        }

        private void OnDisable()
        {
            dropdown.onValueChanged.RemoveListener(OnDropdownValueChanged); 
        }

        private void OnDropdownValueChanged(int index)
        {
            Easing.Ease selectedEase = (Easing.Ease)Enum.Parse(typeof(Easing.Ease), dropdown.options[index].text);
            foreach (SliderCommandController commandController in sliderCommandControllers)
            {
                commandController.SetEasing(selectedEase);
            }
            
            PlayerPrefs.SetInt(PrefsKey, index);
            PlayerPrefs.Save();
        }
    }
}