﻿using _CatBot.FirmwareControllers;
using _CatBot.Math;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _CatBot.Sliders
{
    public class SliderCommandController : MonoBehaviour
    {
        [SerializeField]
        private PointerEventsSlider slider;

        [SerializeField]
        private CatbotFirmwareController deviceController;
        
        [SerializeField]
        private float commandThrottlingSeconds = 0.046f;

        [SerializeField]
        private float deadZone = 0.02f;
        
        [SerializeField]
        private float minValue = 200;

        [SerializeField]
        private float maxValue = 1024;

        [SerializeField]
        [Tooltip("See https://easings.net/en")]
        private Easing.Ease easing = Easing.Ease.Linear;
        
        private bool _pendingSendStop;
        
        public enum ControlSide
        {
            Left,
            Right
        }

        public ControlSide side;
        
        private float _nextRepeatedCommandSend;
        private bool _draggingSlider;
        
        private void OnEnable()
        {
            slider.PointerDown += OnPointerDown;
            slider.PointerUp += OnPointerUp;
        }

        private void OnDisable()
        {
            slider.PointerDown -= OnPointerDown;
            slider.PointerUp -= OnPointerUp;
        }

        private void OnPointerUp(PointerEventData evt)
        {
            slider.ResetSlider();
            _draggingSlider = false;
        }

        private void OnPointerDown(PointerEventData evt)
        {
            _draggingSlider = true;
        }

        private void Update()
        {
            if (!_draggingSlider && _pendingSendStop)
            {
                SendStop();
                _pendingSendStop = false;
                return;
            }
            
            if (Time.time < _nextRepeatedCommandSend)
            {
                return;
            }
            _nextRepeatedCommandSend = Time.time + commandThrottlingSeconds;

            SendMovement();
        }

        private void SendStop()
        {
            if (side == ControlSide.Left)
            {
                deviceController.SetLeftMotor(0);
                return;
            }
            
            deviceController.SetRightMotor(0);
        }
        
        private void SendMovement()
        {
            if (side == ControlSide.Left)
            {
                deviceController.SetLeftMotor((int)MappedSliderValue());
                _pendingSendStop = true;
                return;
            }
            
            deviceController.SetRightMotor((int)MappedSliderValue());
            _pendingSendStop = true;
        }

        private float MappedSliderValue()
        {
            //slider.value is -1 to 1
            
            float absSliderValue = Mathf.Abs(slider.value);// this is always 0 to 1
            if (absSliderValue < deadZone)
            {
                return 0;
            }

            float sign = Mathf.Sign(slider.value);

            var function = Easing.GetEasingFunction(easing);
            return sign * function(minValue, maxValue, absSliderValue);
        }

        public void SetEasing(Easing.Ease newEasing)
        {
            easing = newEasing; 
        }
    }
}