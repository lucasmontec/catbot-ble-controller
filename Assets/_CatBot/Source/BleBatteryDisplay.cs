﻿using _CatBot.FirmwareControllers;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using ZuBle;

namespace _CatBot
{
    public class BleBatteryDisplay : MonoBehaviour
    {
        public float readIntervalSeconds = 1f;

        public float failedRetrySeconds = 0.5f;
        
        [SerializeField]
        private TextMeshProUGUI statusText;

        [SerializeField]
        private CatbotFirmwareController deviceController;
        
        private float _nextReadTime;

        private void Update()
        {
            if (!deviceController.IsConnected)
            {
                return;
            }
            
            if (Time.time < _nextReadTime)
            {
                return;
            }

            _nextReadTime = Time.time + readIntervalSeconds;

            ReadBattery().Forget();
        }

        private async UniTaskVoid ReadBattery()
        {
            var batteryPercentage = await BleManager.ReadBatteryCharacteristic(deviceController.ConnectedDeviceID);
            bool validRead = batteryPercentage >= 0;
            if (validRead)
            {
                statusText.text = $"{batteryPercentage}%";
                return;
            }

            _nextReadTime = Time.time + failedRetrySeconds;
        }
    }
}