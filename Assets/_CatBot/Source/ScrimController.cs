﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _CatBot
{
    public class ScrimController : MonoBehaviour
    {
        [SerializeField]
        private Image scrim;

        [SerializeField]
        private float fadeTime = 0.3f;
        
        private float _scrimDefaultAlpha;

        private void Awake()
        {
            scrim.enabled = true;
            _scrimDefaultAlpha = scrim.color.a;
            var currentColor = scrim.color;
            currentColor.a = 0;
            scrim.color = currentColor;
        }

        public void Show()
        {
            DOTween.Sequence()
                .Append(scrim.DOFade(_scrimDefaultAlpha, fadeTime))
                .AppendCallback(() => scrim.raycastTarget = true);
        }

        public void Hide()
        {
            DOTween.Sequence()
                .Append(scrim.DOFade(0, fadeTime))
                .AppendCallback(() => scrim.raycastTarget = false);
        }
    }
}