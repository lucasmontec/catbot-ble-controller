﻿using _CatBot.FirmwareControllers;
using UnityEngine;
using UnityEngine.UI;

namespace _CatBot.Controllers
{
    public class CatBotCommandButton : MonoBehaviour
    {
        public Button button;
        public string command;
        
        public CatbotFirmwareController catBotController;

        private void OnEnable()
        {
            button.onClick.AddListener(Click);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(Click);
        }

        private void Click()
        {
            catBotController.SendCommand(command);
        }
    }
}