﻿using _CatBot.FirmwareControllers;
using UnityEngine;

namespace _CatBot.Controllers
{
    public class DPadController : MonoBehaviour
    {
        [SerializeField]
        private HoldButton forward;
        [SerializeField]
        private HoldButton backward;
        [SerializeField]
        private HoldButton left;
        [SerializeField]
        private HoldButton right;

        [SerializeField]
        private CatbotFirmwareController deviceController;
        
        private void OnEnable()
        {
            forward.WhilePressed += deviceController.Forward;
            backward.WhilePressed += deviceController.Backward;
            left.WhilePressed += deviceController.TurnLeft;
            right.WhilePressed += deviceController.TurnRight;
        }
        
        private void OnDisable()
        {
            forward.WhilePressed -= deviceController.Forward;
            backward.WhilePressed -= deviceController.Backward;
            left.WhilePressed -= deviceController.TurnLeft;
            right.WhilePressed -= deviceController.TurnRight;
        }
    }
}