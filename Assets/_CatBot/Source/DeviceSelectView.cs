﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _CatBot
{
    public class DeviceSelectView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI deviceNameField;
        [SerializeField]
        private TextMeshProUGUI deviceIdField;
        [SerializeField]
        private Button connect;
        
        private ConnectDeviceAction _connectAction;
        
        public void Setup(string deviceName, string deviceId, ConnectDeviceAction connectCalled)
        {
            deviceNameField.text = deviceName;
            deviceIdField.text = deviceId;
            _connectAction = connectCalled;
        }

        private void OnEnable()
        {
            connect.onClick.AddListener(ConnectClicked);
        }

        private void OnDisable()
        {
            connect.onClick.RemoveListener(ConnectClicked);
        }

        private void ConnectClicked()
        {
            _connectAction?.Invoke(deviceIdField.text, deviceNameField.text);
        }
    }
}