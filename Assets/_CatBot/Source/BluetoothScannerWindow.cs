﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZuBle;

namespace _CatBot
{
    public delegate void ConnectDeviceAction(string deviceUuid, string deviceName);
    
    public class BluetoothScannerWindow : MonoBehaviour
    {
        private const string ReadyToScanStatusText = "Ready to scan.";

        [SerializeField]
        private ScrimController scrim;
        
        [SerializeField]
        private GameObject contentRoot;
        
        [SerializeField]
        private Button scanButton;
        
        [SerializeField]
        private TextMeshProUGUI scanButtonText;

        [SerializeField]
        private TextMeshProUGUI scanStatusField;
        
        [SerializeField]
        private int scanTime = 10;

        [SerializeField]
        private Transform devicesListRoot;

        [SerializeField]
        private DeviceSelectView deviceSelectViewPrefab;

        public bool showOnEnable;
        
        public event Action<string, string> OnConnectCalled;
        
        private bool _isScanning;

        private float _scanEndTime;

        private readonly HashSet<string> _currentDevices = new();
        
        private void Awake()
        {
            BleManager.Initialize();
        }

        public void Show()
        {
            EndScan();
            
            contentRoot.SetActive(true);
            scanStatusField.text = ReadyToScanStatusText;
            scrim.Show();
        }

        public void Hide()
        {
            scanButton.interactable = true;
            contentRoot.SetActive(false);
            scrim.Hide();
            
            BleManager.StopScanning();
        }
        
        private void OnEnable()
        {
            _isScanning = false;
            if (showOnEnable)
            {
                Show();
            }
            scanButton.onClick.AddListener(Scan);
            scanStatusField.text = ReadyToScanStatusText;

            BleManager.OnDeviceFound += OnDeviceFound;
        }

        private void OnDisable()
        {
            scanButton.interactable = true;
            scanButton.onClick.RemoveListener(Scan);
            
            BleManager.OnDeviceFound -= OnDeviceFound;
        }

        private void Scan()
        {
            if (_isScanning)
            {
                _isScanning = false;
                scanButtonText.text = "Scan";
                BleManager.StopScanning();
                return;
            }
            
            _currentDevices.Clear();
            
            _isScanning = true;
            scanButtonText.text = "Stop";

            DestroyAllPreviousResultViews();
            
            BleManager.StopScanning();
            BleManager.StartScanning();

            _scanEndTime = Time.time + scanTime;
            scanStatusField.text = $"Scanning for {scanTime} seconds.";
            
            Application.logMessageReceived += OnLog;
        }

        private void OnLog(string message, string stacktrace, LogType type)
        {
            if (type is LogType.Error or LogType.Exception)
            {
                scanStatusField.text = $"Error: {message}.";
            }
        }

        private void DestroyAllPreviousResultViews()
        {
            foreach (Transform child in devicesListRoot)
            {
                Destroy(child.gameObject);
            }
        }

        private void Update()
        {
            CheckScanEnd();
        }

        private void CheckScanEnd()
        {
            if (!_isScanning) return;
            if (Time.time < _scanEndTime)
            {
                return;
            }
            EndScan();
        }

        private void EndScan()
        {
            BleManager.StopScanning();
            _scanEndTime = -1f;
            _isScanning = false;
            scanStatusField.text = ReadyToScanStatusText;
            scanButton.interactable = true;
            Application.logMessageReceived -= OnLog;
            _currentDevices.Clear();
        }

        private void OnDeviceFound(BleDevice bleDevice)
        {
            if (!_currentDevices.Add(bleDevice.Address))
            {
                return;
            }

            scanStatusField.text = $"Device found: {bleDevice.Name}!";
            
            DeviceSelectView deviceView = Instantiate(deviceSelectViewPrefab, devicesListRoot);
            deviceView.Setup(bleDevice.Name, bleDevice.Address, ConnectToDevice);
        }

        private void ConnectToDevice(string deviceId, string deviceName)
        {
            OnConnectCalled?.Invoke(deviceId, deviceName);
        }
    }
}