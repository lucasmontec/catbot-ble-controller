using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace _CatBot.FirmwareControllers
{
    public class InputStateBLEDeviceController : CatbotFirmwareController
    {
        //private const string STOP_COMMAND = "stp";
        
        [SerializeField]
        private Toggle tankModeToggle;

        private bool _forward, _backward, _left, _right;
        private int _tankLeft, _tankRight;
        private bool _isTankMode;

        private bool _standby;

        private bool AnyTank => !Mathf.Approximately(_tankRight, 0) || !Mathf.Approximately(_tankLeft, 0);
        
        protected override bool PressingAny => _forward || _backward || _left || _right || 
                                               _isTankMode && AnyTank;

        protected override void OnEnable()
        {
            base.OnEnable();
            
            tankModeToggle.onValueChanged.AddListener(TankModeToggled);
            TankModeToggled(tankModeToggle.isOn);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            tankModeToggle.onValueChanged.RemoveListener(TankModeToggled);
        }

        private void LateUpdate()
        {
            if (PressingAny)
            {
                _standby = false;
                
                string command = _isTankMode ?
                    $"{_tankLeft}:{_tankRight}" :
                    BuildInputState();
                
                SendControlCommandThrottle(command);
                return;
            }

            _forward = false;
            _backward = false;
            _right = false;
            _left = false;
            
            if(_standby)
            {
                return;
            }

            _standby = true;
            SendControlCommand(StandbyCommand);
        }
        
        private void TankModeToggled(bool mode)
        {
            _isTankMode = mode;
        }

        private string BuildInputState()
        {
            StringBuilder stateCommand = new();
            
            if (_forward)
            {
                stateCommand.Append('f');
            }
            if (_backward)
            {
                stateCommand.Append('b');
            }
            if (_left)
            {
                stateCommand.Append('l');
            }
            if (_right)
            {
                stateCommand.Append('r');
            }

            return stateCommand.ToString();
        }

        public override void TurnRight()
        {
            _right = true;
        }

        public override void TurnLeft()
        {
            _right = false;
            _left = true;
        }

        public override void Backward()
        {
            _backward = true;
        }

        public override void Forward()
        {
            _forward = true;
            _backward = false;
        }

        public override void SetLeftMotor(int value)
        {
            _tankLeft = value;
        }
        
        public override void SetRightMotor(int value)
        {
            _tankRight = value;
        }

        public override void BrakeA()
        {
            //Not needed anymore
            //SendControlCommand(STOP_COMMAND+"a");
        }

        public override void BrakeB()
        {
            //Not needed anymore
            //SendControlCommand(STOP_COMMAND+"b");
        }

        public override void Stop()
        {
            SendControlCommand(StandbyCommand);
            
            //Stop causes wheels to roll back
            //SendControlCommand(STOP_COMMAND);
        }
    }
}
