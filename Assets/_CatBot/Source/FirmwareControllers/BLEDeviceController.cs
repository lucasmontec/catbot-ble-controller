namespace _CatBot.FirmwareControllers
{
    public class BLEDeviceController : CatbotFirmwareController
    {
        private string _lastCommand;
        private float _nextRepeatedCommandSend;
        private bool _pressingAny;

        protected override bool PressingAny => _pressingAny;

        private void LateUpdate()
        {
            if (!_pressingAny)
            {
                return;
            }
            _pressingAny = false;
            SendControlCommand(StandbyCommand);//standby
        }

        public override void TurnRight()
        {
            SendControlCommandThrottle("r");
            _pressingAny = true;
        }

        public override void TurnLeft()
        {
            SendControlCommandThrottle("l");
            _pressingAny = true;
        }

        public override void Backward()
        {
            SendControlCommandThrottle("b");
            _pressingAny = true;
        }

        public override void Forward()
        {
            SendControlCommandThrottle("f");
            _pressingAny = true;
        }

        public override void SetLeftMotor(int value)
        {
            SendControlCommand($"mb {value}");
            _pressingAny = true;
        }
        
        public override void SetRightMotor(int value)
        {
            SendControlCommand($"ma {value}");
            _pressingAny = true;
        }

        public override void BrakeA()
        {
            SendControlCommand("bra");
        }
        
        public override void BrakeB()
        {
            SendControlCommand("brb");
        }

        public override void Stop()
        {
            SendControlCommand("br");
        }
    }
}
