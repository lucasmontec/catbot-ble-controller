﻿using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZuBle;

namespace _CatBot.FirmwareControllers
{
    public abstract class CatbotFirmwareController : MonoBehaviour
    {
        protected const string StandbyCommand = "stb";
        
        [SerializeField]
        private Image statusImage;

        [SerializeField]
        private TextMeshProUGUI statusText;

        [SerializeField]
        private BluetoothScannerWindow bluetoothScannerWindow;

        [SerializeField]
        private float repeatedCommandSendInterval = 0.04f;

        private string _lastCommand;
        private float _nextRepeatedCommandSend;
        
        private const string ServiceUuid = "25e1362d-af97-42c1-92e1-41739cc194e6";
        private const string CharacteristicUuid = "2a7b25d1-cf73-4b3d-bd95-3a44c1646d1c";
        public string ConnectedDeviceID { get; private set; } = string.Empty;
        public bool IsConnected { get; private set; }

        protected abstract bool PressingAny { get; }

        protected virtual void OnEnable()
        {
            bluetoothScannerWindow.OnConnectCalled += ConnectCalled;
            
            BleManager.OnConnected += OnConnected;
            BleManager.OnDisconnected += OnDisconnected;
        }

        protected virtual void OnDisable()
        {
            bluetoothScannerWindow.OnConnectCalled -= ConnectCalled;
            
            BleManager.OnConnected -= OnConnected;
            BleManager.OnDisconnected -= OnDisconnected;
        }

        private void Disconnect()
        {
            BleManager.DisconnectDevice(ConnectedDeviceID);
            bluetoothScannerWindow.Show();
        }

        private void ConnectCalled(string deviceId, string deviceName)
        {
            if (IsConnected)
            {
                if (ConnectedDeviceID != deviceId)
                {
                    Disconnect();
                }
                else
                {
                    return;
                }
            }

            bluetoothScannerWindow.Hide();
            
            ConnectedDeviceID = deviceId;

            BleManager.ConnectToDevice(ConnectedDeviceID).Forget();
        }

        private void OnDisconnected(string deviceAddress)
        {
            ShowDisconnectedStatus();
            IsConnected = false;
            bluetoothScannerWindow.Show();
        }

        private void OnConnected(string deviceAddress)
        {
            ShowConnectedStatus();
            IsConnected = true;
        }

        public abstract void TurnRight();
        public abstract void TurnLeft();
        public abstract void Backward();
        public abstract void Forward();
        public abstract void SetLeftMotor(int value);
        public abstract void SetRightMotor(int value);

        public abstract void BrakeA();
        
        public abstract void BrakeB();

        public abstract void Stop();

        public void SendControlCommandThrottle(string command)
        {
            if (!IsConnected)
            {
                return;
            }
            
            if (command == _lastCommand && 
                Time.time < _nextRepeatedCommandSend)
            {
                return;
            }
            _lastCommand = command;
            _nextRepeatedCommandSend = Time.time + repeatedCommandSendInterval;

            SendControlCommand(command);
        }

        public void SendControlCommand(string command)
        {
            if (!IsConnected)
            {
                return;
            }

            BleManager.WriteCharacteristic(ConnectedDeviceID, ServiceUuid, CharacteristicUuid, $"!{command}");
        }
        
        public void SendCommand(string command)
        {
            if (!IsConnected)
            {
                return;
            }

            BleManager.WriteCharacteristic(ConnectedDeviceID, ServiceUuid, CharacteristicUuid, command);
        }

        private void ShowConnectedStatus()
        {
            statusImage.color = Color.green;
            statusText.text = "Connected";
        }

        private void ShowDisconnectedStatus()
        {
            statusImage.color = Color.red;
            statusText.text = "Disconnected";
        }
    }
}